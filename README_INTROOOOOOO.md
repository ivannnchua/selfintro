# Self Introduction 

Self introduction for IDP Exercise 02. Hi my name is Ivan ;)

## Name
Ivan Chua Chi Whye 

## How Do I Look irl lol
<!-- ![alt text](ThisPC/Pictures/86665845_1255370571315157_6334256568875352064_n.jpg) -->
<img src="https://gitlab.com/ivannnchua/selfintro/uploads/460fdc6c651cc7e2a3f5c5bcb567e7cb/86665845_1255370571315157_6334256568875352064_n.jpg" alt="me" width="200px" height="auto">

## I'm From
Kuala Lumpur, Malaysia

## Strength and Weaknesses

| No. | Strength | Weaknesses |
| ------ | ------ | ------ | 
| 1 | Relatively good time management | Hate people who aren't punctual lol | 
| 2 | No stage fright | Bad at socialising | 
| 3 | Good at theoretical stuff | Bad in progamming  |

## A List of Stuff I Like and Dislike
- I like to sleep. 
- I like to eat.
- I dislike dark places lmao


### That's it, thank you. 
